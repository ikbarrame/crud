@extends('adminlte.master')

@section('content')

    <div class="ml-3" >

        <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
                </div>
                
                <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST" >
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" name="judul" value=" {{ old('judul', $pertanyaan->judul) }} " id="judul" placeholder="Masukan Judul" required >
                        @error('judul')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" value=" {{ old('isi', $pertanyaan->isi) }}  " name="isi" placeholder="Isi" required cols="30" rows="10">
                    <!-- <textarea type="text" class="form-control" id="isi" value=" {{ old('isi', $pertanyaan->isi) }}  " name="isi" placeholder="Isi" required cols="30" rows="10"></textarea> -->
                         @error('isi')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
               

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Edit</button>
                </div>
                </form>
            </div>



    </div>


@endsection
